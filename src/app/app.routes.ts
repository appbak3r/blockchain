import { Routes } from '@angular/router';

import { MainComponent } from './main/components/main.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
];
