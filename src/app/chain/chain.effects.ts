import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { ChainApiService, IChainItem } from './services/api/chain.api.service';

import { ActionTypes, GetChainAction, GetChainErrorAction, GetChainSuccessAction } from './chain.actions';

@Injectable()
export class ChainEffects {
  @Effect()
  get: Observable<Action> = this.actions
    .ofType(ActionTypes.GET)
    .pipe(map((action: GetChainAction) => action.payload))
    .pipe(switchMap((payload: any) => {
      return this.chainApiService.getChain()
        .pipe((map((data: IChainItem[]) => {
          return new GetChainSuccessAction(data);
        })), catchError(() => {
          return of(new GetChainErrorAction({}));
        }));
    }));

  constructor (
    private actions: Actions,
    private chainApiService: ChainApiService,
  ) {
  }
}
