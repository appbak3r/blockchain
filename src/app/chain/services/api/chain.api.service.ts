import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

export interface IChainItem {
  id: string;
  interlinks: string[];
  showInterlinks?: boolean;
}

@Injectable()
export class ChainApiService {
  constructor (private http: HttpClient) {
  }

  getChain (): Observable<IChainItem[]> {
    return this.http.get<IChainItem[]>('/assets/chain.json');
  }
}
