import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ActionTypes, Actions } from './chain.actions';

import { IChainItem } from './services/api/chain.api.service';

export interface State {
  isLoading: boolean;
  isLoaded: boolean;

  error?: string;
  blocks: IChainItem[];
}

const initialState: State = {
  isLoaded: false,
  isLoading: false,
  blocks: [],
};

export function reducer (state: State = initialState, action: Actions) {
  switch (action.type) {
    case ActionTypes.GET: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case ActionTypes.GET_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        blocks: action.payload,
      };
    }

    case ActionTypes.GET_ERROR: {
      return {
        ...state,
        isLoaded: false,
        isLoading: false,
        error: 'Error loading blocks',
      };
    }
  }

  return state;
}


export const selectChain = createFeatureSelector<State>('chain');
export const blocks      = createSelector(selectChain, (state: State) => state.blocks);
export const blockHash   = createSelector(blocks, (blockState: IChainItem[]) => {
  const hashMap = {};

  blockState.forEach((item: IChainItem) => {
    return hashMap[item.id] = item.interlinks;
  });

  return hashMap;
});
