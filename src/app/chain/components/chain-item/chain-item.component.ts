import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IChainItem } from '../../services/api/chain.api.service';

@Component({
  selector: 'app-chain-item',
  templateUrl: 'chain-item.component.html',
  styleUrls: ['chain-item.css'],
})
export class ChainItemComponent {
  @Input('item') item: IChainItem;
  @Output() onShowSubInterLinks = new EventEmitter<any>();

  showSubInterlinks (chainItem: IChainItem, level: number) {
    if (!this.item.showInterlinks) {
      this.onShowSubInterLinks.emit({ chainItem, level });
    }
  }
}
