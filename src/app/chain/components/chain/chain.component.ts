import { Component, ElementRef, Input, OnChanges } from '@angular/core';
import { IChainItem } from '../../services/api/chain.api.service';

@Component({
  selector: 'app-chain',
  templateUrl: 'chain.component.html',
  styleUrls: ['chain.css'],
})
export class ChainComponent implements OnChanges {
  @Input() items: IChainItem[];
  @Input() hashItems: {};
           canGoBack: boolean;

  displayItems: IChainItem[];

  ngOnChanges (): void {
    this.displayItems = this.items;
  }

  reset () {
    this.canGoBack    = false;
    this.displayItems = this.items;
  }

  showSubInterLinks ({ chainItem, level }: { chainItem: IChainItem, level: number }): void {
    const selectedItems = [];

    let item = this.hashItems[chainItem.id];

    while (item) {
      if (item[level]) {
        selectedItems.push(item[level]);

        item = this.hashItems[item[level]];
      } else {
        item = null;
      }
    }

    this.displayItems = [
      {
        id: chainItem.id,
        interlinks: selectedItems,
        showInterlinks: true,
      },
    ];

    this.canGoBack = true;
  }
}
