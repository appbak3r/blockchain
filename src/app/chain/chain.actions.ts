import { Action } from '@ngrx/store';

import { type } from '../utils/type';

export const ActionTypes: any = {
  GET: type('[chain] get'),
  GET_SUCCESS: type('[chain] get success'),
  GET_ERROR: type('[chain] get error'),
};

export class GetChainAction implements Action {
  readonly type: string = ActionTypes.GET;

  constructor (public payload?: any) {
  }
}

export class GetChainSuccessAction implements Action {
  readonly type: string = ActionTypes.GET_SUCCESS;

  constructor (public payload: any) {
  }
}

export class GetChainErrorAction implements Action {
  readonly type: string = ActionTypes.GET_ERROR;

  constructor (public payload?: any) {
  }
}

export type Actions
  = GetChainAction
  | GetChainSuccessAction
  | GetChainErrorAction;
