import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { ChainEffects } from './chain.effects';

import { ChainItemComponent } from './components/chain-item/chain-item.component';
import { ChainComponent } from './components/chain/chain.component';

import { ChainApiService } from './services/api/chain.api.service';

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([
      ChainEffects,
    ]),
  ],
  declarations: [
    ChainComponent,
    ChainItemComponent,
  ],
  providers: [
    ChainApiService,
  ],
  exports: [
    ChainComponent,
  ],
})
export class ChainModule {}
