import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import { State } from '../../app.reducers';
import { GetChainAction } from '../../chain/chain.actions';
import { blockHash, blocks } from '../../chain/chain.reducers';
import { IChainItem } from '../../chain/services/api/chain.api.service';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
})
export class MainComponent implements OnInit {
  chainItems: IChainItem[] = [];
  hashItems: {};


  constructor (
    private store: Store<State>,
  ) {
    this.store.select(blocks)
      .subscribe((chainItems: IChainItem[]) => {
        this.chainItems = chainItems;
      });

    this.store.select(blockHash)
      .subscribe((items: {}) => {
        this.hashItems = items;
      });
  }

  ngOnInit (): void {
    this.store.dispatch(new GetChainAction());
  }
}

