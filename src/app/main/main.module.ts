import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChainModule } from '../chain/chain.module';

import { MainComponent } from './components/main.component';
import { AppComponent } from './containers/app.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ChainModule,
  ],
  declarations: [
    MainComponent,
    AppComponent,
  ],
  exports: [
    AppComponent,
  ],
})
export class MainModule {}
