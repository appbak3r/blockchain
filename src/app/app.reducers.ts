import { RouterState } from '@angular/router';
import { routerReducer } from '@ngrx/router-store';

import * as chain from './chain/chain.reducers';

export interface State {
  router: RouterState;
  chain: chain.State;
}

export const appReducers = {
  router: routerReducer,
  chain: chain.reducer,
};
